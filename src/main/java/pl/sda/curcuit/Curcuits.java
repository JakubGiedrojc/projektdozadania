package main.java.pl.sda.curcuit;

public class Curcuits {

    public static void CurcuitCalc(float curcuit){
        float curcuitcal = (float) (curcuit*3.14);
        System.out.println("Obwód koła to "+curcuitcal);
    }
    public static void CurcuitCalcExact(float curcuit){
        float curcuitcalcexact= (float) (curcuit*Math.PI);
        System.out.println("Obwód koła to "+curcuitcalcexact);
    }
}
